#ifndef __NAND_PHYSIC_FUN_H__
#define __NAND_PHYSIC_FUN_H__

extern __s32 m0_spi_nand_reset(__u32 spi_no, __u32 chip);
extern __s32 m0_spi_nand_read_status(__u32 spi_no, __u32 chip, __u8 status, __u32 mode);
extern __s32 m0_spi_nand_setstatus(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m0_spi_nand_getblocklock(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m0_spi_nand_setblocklock(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m0_spi_nand_getotp(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m0_spi_nand_setotp(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m0_spi_nand_getoutdriver(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m0_spi_nand_setoutdriver(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m0_erase_single_block(struct boot_physical_param *eraseop);
extern __s32 m0_write_single_page (struct boot_physical_param *writeop );
extern __s32 m0_read_single_page(struct boot_physical_param *readop, __u32 spare_only_flag);


extern __s32 m1_spi_nand_reset(__u32 spi_no, __u32 chip);
extern __s32 m1_spi_nand_read_status(__u32 spi_no, __u32 chip, __u8 status, __u32 mode);
extern __s32 m1_spi_nand_setstatus(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m1_spi_nand_getblocklock(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m1_spi_nand_setblocklock(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m1_spi_nand_getotp(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m1_spi_nand_setotp(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m1_spi_nand_getoutdriver(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m1_spi_nand_setoutdriver(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m1_erase_single_block(struct boot_physical_param *eraseop);
extern __s32 m1_write_single_page (struct boot_physical_param *writeop );
extern __s32 m1_read_single_page(struct boot_physical_param *readop, __u32 spare_only_flag);


extern __s32 m2_spi_nand_reset(__u32 spi_no, __u32 chip);
extern __s32 m2_spi_nand_read_status(__u32 spi_no, __u32 chip, __u8 status, __u32 mode);
extern __s32 m2_spi_nand_setstatus(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m2_spi_nand_getblocklock(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m2_spi_nand_setblocklock(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m2_spi_nand_getotp(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m2_spi_nand_setotp(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m2_spi_nand_getoutdriver(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m2_spi_nand_setoutdriver(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m2_erase_single_block(struct boot_physical_param *eraseop);
extern __s32 m2_write_single_page (struct boot_physical_param *writeop );
extern __s32 m2_read_single_page(struct boot_physical_param *readop, __u32 spare_only_flag);


extern __s32 m3_spi_nand_reset(__u32 spi_no, __u32 chip);
extern __s32 m3_spi_nand_read_status(__u32 spi_no, __u32 chip, __u8 status, __u32 mode);
extern __s32 m3_spi_nand_setstatus(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m3_spi_nand_getblocklock(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m3_spi_nand_setblocklock(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m3_spi_nand_getotp(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m3_spi_nand_setotp(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m3_spi_nand_getoutdriver(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m3_spi_nand_setoutdriver(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m3_erase_single_block(struct boot_physical_param *eraseop);
extern __s32 m3_write_single_page (struct boot_physical_param *writeop );
extern __s32 m3_read_single_page(struct boot_physical_param *readop, __u32 spare_only_flag);

extern __s32 m4_spi_nand_reset(__u32 spi_no, __u32 chip);
extern __s32 m4_spi_nand_read_status(__u32 spi_no, __u32 chip, __u8 status, __u32 mode);
extern __s32 m4_spi_nand_setstatus(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m4_spi_nand_getblocklock(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m4_spi_nand_setblocklock(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m4_spi_nand_getotp(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m4_spi_nand_setotp(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m4_spi_nand_getoutdriver(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m4_spi_nand_setoutdriver(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m4_erase_single_block(struct boot_physical_param *eraseop);
extern __s32 m4_write_single_page (struct boot_physical_param *writeop );
extern __s32 m4_read_single_page(struct boot_physical_param *readop, __u32 spare_only_flag);

extern __s32 m5_spi_nand_reset(__u32 spi_no, __u32 chip);
extern __s32 m5_spi_nand_read_status(__u32 spi_no, __u32 chip, __u8 status, __u32 mode);
extern __s32 m5_spi_nand_setstatus(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m5_spi_nand_getblocklock(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m5_spi_nand_setblocklock(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m5_spi_nand_getotp(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m5_spi_nand_setotp(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m5_spi_nand_getoutdriver(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m5_spi_nand_setoutdriver(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m5_erase_single_block(struct boot_physical_param *eraseop);
extern __s32 m5_write_single_page (struct boot_physical_param *writeop );
extern __s32 m5_read_single_page(struct boot_physical_param *readop, __u32 spare_only_flag);


extern __s32 m6_spi_nand_reset(__u32 spi_no, __u32 chip);
extern __s32 m6_spi_nand_read_status(__u32 spi_no, __u32 chip, __u8 status, __u32 mode);
extern __s32 m6_spi_nand_setstatus(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m6_spi_nand_getblocklock(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m6_spi_nand_setblocklock(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m6_spi_nand_getotp(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m6_spi_nand_setotp(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m6_spi_nand_getoutdriver(__u32 spi_no, __u32 chip, __u8* reg);
extern __s32 m6_spi_nand_setoutdriver(__u32 spi_no, __u32 chip, __u8 reg);
extern __s32 m6_erase_single_block(struct boot_physical_param *eraseop);
extern __s32 m6_write_single_page (struct boot_physical_param *writeop );
extern __s32 m6_read_single_page(struct boot_physical_param *readop, __u32 spare_only_flag);

#endif

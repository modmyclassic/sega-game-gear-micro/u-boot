/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_readretry.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR5_C_

#include "../nand_physic_inc.h"

u8 m5_read_retry_mode = 0;
u8 m5_read_retry_cycle = 0;
u8 m5_read_retry_addr[4] = {0xa7,0xa4,0xa5,0xa6};
u8 m5_read_retry_cmd[4] = {0xa1,0xa1,0xa1,0xa1};

u8 m5_p1[15][4] ={
    {0x00, 0x00, 0x00, 0x00},    //0
    {0x05, 0x0A, 0x00, 0x00},    //1
    {0x28, 0x00, 0xEC, 0xD8},    //2
    {0xED, 0xF5, 0xED, 0xE6},    //3
    {0x0A, 0x0F, 0x05, 0x00},    //4
    {0x0F, 0x0A, 0xFB, 0xEC},    //5
    {0xE8, 0xEF, 0xE8, 0xDC},    //6
    {0xF1, 0xFB, 0xFE, 0xF0},    //7
    {0x0A, 0x00, 0xFB, 0xEC},    //8
    {0xD0, 0xE2, 0xD0, 0xC2},    //9
    {0x14, 0x0F, 0xFB, 0xEC},    //10
    {0xE8, 0xFB, 0xE8, 0xDC},    //11
    {0x1E, 0x14, 0xFB, 0xEC},    //12
    {0xFB, 0xFF, 0xFB, 0xF8},    //13
    {0x07, 0x0C, 0x02, 0x00}     //14
};
/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m5_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);
    if(ret == ERR_ECC)
	{
	    PHY_DBG("m5 retry!\n");
	    for(i=1; i<m5_read_retry_cycle; i++)
	    {
	        nci->retry_count = i;
	        ret = m5_set_readretry(nci);
	        if(ret != 0)
	        {
	            continue;
	        }

	        ret = m0_read_page_start(npo);
	        ret |= m0_read_page_end_not_retry(npo);
	        if((ret == ECC_LIMIT) || (ret == 0))
	        {
				ret = ECC_LIMIT;
	            PHY_DBG("m5 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
	            break;
	        }
	    }
	    nci->retry_count = 0;
        m5_set_readretry(nci);
	}

//	if((ret != 0) && (ret != ECC_LIMIT))
//	{
//	    PHY_DBG("m5 ReadRetry fail! ch =%d, chip = %d  block = %d, page = %d\n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page);
//	}

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m5_readretry_init(struct _nand_chip_info *nci)
{
	nci->retry_count = 0;

    m5_read_retry_mode = (nci->npi->read_retry_type >> 16) & 0xff;
    m5_read_retry_cycle = 15;

    PHY_DBG("samsung read retry conut: %d !\n",m5_read_retry_cycle);

    if(m5_read_retry_mode != 0x20)
    {
        return ERR_NO_100;
    }

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m5_vender_set_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    s32 ret;
    ret = set_cmd_with_nand_bus(nci,m5_read_retry_cmd,0,addr,para,1,count);
    PHY_DBG("rr value %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m5_readretry_exit(struct _nand_chip_info *nci)
{
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m5_set_readretry(struct _nand_chip_info *nci)
{
    s32 ret;

    nand_enable_chip(nci);

    ndfc_disable_randomize(nci->nctri);

    ret = m5_vender_set_param(nci,m5_p1[nci->retry_count],m5_read_retry_addr,4);
    if(ret != 0)
    {
       PHY_ERR("m5 set readretry error ! %x \n",nci->retry_count);
    }

	nand_disable_chip(nci);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m5_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m5_readretry_init(nci);
		nci = nci->nsi_next;
	}

    if(ret == 0)
    {
	    function_read_page_end = m5_read_page_end;
	    PHY_DBG(" m5_special_init m5_read_retry_mode:%d m5_read_retry_cycle :%d \n",m5_read_retry_mode,m5_read_retry_cycle);
	}
	else
	{
	    PHY_ERR(" m5_special_init error m5_read_retry_mode:%d m5_read_retry_cycle :%d \n",m5_read_retry_mode,m5_read_retry_cycle);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m5_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
		m5_readretry_exit(nci);
		nci = nci->nsi_next;
	}
	PHY_DBG(" m5_special_exit \n");
    return 0;
}

int m5_is_lsb_page(__u32 page_num)
{	
	struct _nand_chip_info *nci = g_nsi->nci;

	//(NAND_LSBPAGE_TYPE == 0x20) //samsung 25nm
	if(page_num==0)
		return 1;
	if(page_num==nci->npi->page_cnt_per_blk - 1)
		return 0;
	if(page_num%2==1)
		return 1;
	return 0;
}


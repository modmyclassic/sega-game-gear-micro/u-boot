/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR7_C_

#include "../nand_physic_inc.h"

u8 m7_read_retry_mode = 0;
u8 m7_read_retry_cycle = 0;
u8 m7_read_retry_reg_cnt = 0;

u8 m7_acti_start_cmd[2] = {0x3b,0xb9};
u8 m7_acti_start_addr[9] = {0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c};

////////////////////////////////////////////////////////////
u8 m7_read_retry_enable_cmd_19[1] = {0xB6};
u8 m7_read_retry_disable_cmd_19[1] = {0xD6};

u8 m7_read_retry_cmd_19[3] =  {0x53,0x53,0x53};
u8 m7_read_retry_reg_adr_19[9] = {0x04,0x05,0x07};
u8 m7_19_low[16][3] ={
    {0xF0,0x00,0XF0},
    {0xE0,0x00,0XE0},
    {0xD0,0x00,0XD0},
    {0x10,0x00,0X10},
    {0x20,0x00,0X20},
    {0x30,0x00,0X30},
    {0xC0,0x00,0XD0},
    {0x00,0x00,0X10},
    {0x00,0x00,0X20},
    {0x10,0x00,0X20},
    {0xB0,0x00,0XD0},
    {0xA0,0x00,0XD0},
    {0x90,0x00,0XD0},
    {0xB0,0x00,0XC0},
    {0xA0,0x00,0XC0},
    {0x90,0x00,0XC0}
};
u8 m7_19_high[20][3] ={
    {0x00,0XF0,0x00},
    {0x0F,0XE0,0x00},
    {0x0F,0XD0,0x00},
    {0x0E,0XE0,0x00},
    {0x0E,0XD0,0x00},
    {0x0D,0XF0,0x00},
    {0x0D,0XE0,0x00},
    {0x0D,0XD0,0x00},
    {0x01,0X10,0x00},
    {0x02,0X20,0x00},
    {0x02,0X10,0x00},
    {0x03,0X20,0x00},
    {0x0F,0X00,0x00},
    {0x0E,0XF0,0x00},
    {0x0D,0XC0,0x00},
    {0x0F,0XF0,0x00},
    {0x01,0X00,0x00},
    {0x02,0X00,0x00},
    {0x0D,0XB0,0x00},
    {0x0C,0XA0,0x00}
};
////////////////////////////////////////////////////////////
u8 m7_read_retry_cmd_24[3] =  {0x53,0x53,0x53};
u8 m7_read_retry_reg_adr_24[9] = {0x04,0x05,0x07};
u8 m7_24[9][3] = {
    {0x00,0XF0,0x00},
    {0x00,0XE0,0x00},
    {0xFF,0XF0,0xF0},
    {0xEE,0XE0,0xE0},
    {0xDE,0XD0,0xD0},
    {0xCD,0XC0,0xC0},
    {0x01,0X00,0x00},
    {0x02,0X00,0x00},
    {0x03,0X00,0x00},
};

////////////////////////////////////////////////////////////
u8 m7_read_retry_enable_cmd_1y[1] = {0x5D};
u8 m7_read_retry_exit_cmd_1y[1] = {0xFF};

u8 m7_read_retry_cmd_1y[1] =  {0xEF};
u8 m7_read_retry_reg_adr_1y[1] = {0x11};
u8 m7_1y[19][4] ={
    {0x04,0x00,0x7C,0x7C},
    {0x08,0x00,0x04,0x04},
    {0x7C,0x7C,0x00,0x7C},
    {0x7C,0x7C,0x00,0x00},
    {0x0C,0x00,0x78,0x78},
    {0x10,0x00,0x08,0x08},
    {0x7C,0x7C,0x78,0x78},
    {0x7C,0x7C,0x04,0x04},
    {0x7C,0x7C,0x78,0x74},
    {0x14,0x04,0x04,0x00},
    {0x78,0x78,0x00,0x7C},
    {0x78,0x78,0x78,0x78},
    {0x78,0x78,0x78,0x74},
    {0x78,0x78,0x04,0x00},
    {0x78,0x74,0x78,0x74},
    {0x78,0x74,0x74,0x70},
    {0x78,0x74,0x78,0x78},
    {0x78,0x70,0x78,0x74},
    {0x78,0x70,0x70,0x70}
};
////////////////////////////////////////////////////////////

static u32 m7_sclk0_bak = 0;
static u32  m7_sclk1_bak = 0;
static u32  m7_toggle_mode_flag = 0;

s32 m7_exit_readretry(struct _nand_chip_info *nci);
extern int m7_end_one_read_retry(struct _nand_chip_info *nci);
extern int is_nouse_page(u8* buf);
/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m7_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);
    if(ret == ERR_ECC)
	{
	    PHY_DBG("m7 retry!\n");
	    for(i=0; i<m7_read_retry_cycle; i++)
	    {
	        nci->retry_count = i;
	        ret = m7_set_readretry(nci,npo->page);
	        if(ret != 0)
	        {
	            continue;
	        }

	        ret = m0_read_page_start(npo);
	        ret |= m0_read_page_end_not_retry(npo);
	        m7_end_one_read_retry(nci);

            if(is_nouse_page(npo->sdata) == 1)
            {
                ret = ERR_ECC;
                PHY_DBG("retry spare all 0xff! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
                continue;
            }

	        if((ret == ECC_LIMIT) || (ret == 0))
	        {
				ret = ECC_LIMIT;
	            PHY_DBG("m7 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
	            break;
	        }
	    }
	    nci->retry_count = 0;
        m7_exit_readretry(nci);
	}
//	if((ret != 0) && (ret != ECC_LIMIT))
//	{
//	    PHY_DBG("m7 ReadRetry fail! ch =%d, chip = %d  block = %d, page = %d\n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page);
//	}

	return ret;
}
/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m7_activation_seq(struct _nand_chip_info *nci)
{
    s32 ret;
    u8 cmd[9] = {0x53,0x53,0x53,0x53,0x53,0x53,0x53,0x53,0x53};
    u8 dat[9] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    ret = set_cmd_with_nand_bus( nci,m7_acti_start_cmd,0,NULL,NULL,0,2);
    ret |= set_cmd_with_nand_bus( nci,cmd,0,m7_acti_start_addr,dat,1,9);

    nand_disable_chip(nci);

    if(ret != 0)
    {
        PHY_ERR("m7 activation seq fail 0x%x\n", ret);
    }
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m7_readretry_init(struct _nand_chip_info *nci)
{
	nci->retry_count = 0;

    m7_read_retry_mode = (nci->npi->read_retry_type >> 16) & 0xff;
    m7_read_retry_cycle = (nci->npi->read_retry_type >> 8) & 0xff;
    m7_read_retry_reg_cnt = nci->npi->read_retry_type & 0xff;

    if((m7_read_retry_mode == 0x30) || (m7_read_retry_mode == 0x31))
    {
        m7_activation_seq(nci);
    }

    if((m7_read_retry_mode != 0x30) && (m7_read_retry_mode != 0x31) && (m7_read_retry_mode != 0x32))
    {
        return ERR_NO_99;
    }

    PHY_DBG("sandisk read retry conut: %d !\n",m7_read_retry_cycle);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m7_readretry_exit(struct _nand_chip_info *nci)
{
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m7_read_retry_clock_save(struct _nand_chip_info *nci)
{
    NAND_GetClk(nci->nctri->channel_id, &m7_sclk0_bak, &m7_sclk1_bak);
    NAND_SetClk(nci->nctri->channel_id, 10, 10*2);
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m7_read_retry_clock_recover(struct _nand_chip_info *nci)
{
    NAND_SetClk(nci->nctri->channel_id, m7_sclk0_bak, m7_sclk1_bak);
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m7_end_one_read_retry(struct _nand_chip_info *nci)
{
    if(!((m7_read_retry_mode == 0x30) || (m7_read_retry_mode == 0x31)))
    {
        return 0;
    }

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    if(ndfc_is_toogle_interface(nci->nctri) != 0) //change to legacy mode from toggle mode after 0x53h cmd
    {
        ndfc_set_legacy_interface(nci->nctri);
        m7_toggle_mode_flag = 1;
    }

    set_cmd_with_nand_bus(nci,m7_read_retry_disable_cmd_19,0,NULL,NULL,0,1);

    if(m7_toggle_mode_flag == 1)
    {
        ndfc_set_toogle_interface(nci->nctri); //change to toggle mode from legacy mode  after set param
    }

	nand_disable_chip(nci);

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m7_vender_set_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    s32 ret = 0;

    if((m7_read_retry_mode == 0x30) || (m7_read_retry_mode == 0x31))
    {
        ret =  set_cmd_with_nand_bus(nci,m7_acti_start_cmd,0,NULL,NULL,0,2);
        ret |= set_cmd_with_nand_bus(nci,m7_read_retry_cmd_19,0,addr,para,1,3);
	}
	if(m7_read_retry_mode == 0x32)
	{
	    ret = set_cmd_with_nand_bus(nci,m7_read_retry_cmd_1y,1,addr,para,4,1);
	}

//	PHY_DBG("rr value %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m7_set_readretry(struct _nand_chip_info *nci,u16 page)
{
    s32 ret = 0;
    u8* dat;
    u8* addr;
    u32 cnt;

    m7_read_retry_clock_save(nci);

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    if(ndfc_is_toogle_interface(nci->nctri) != 0) //change to legacy mode from toggle mode after 0x53h cmd
    {
        ndfc_set_legacy_interface(nci->nctri);
        m7_toggle_mode_flag = 1;
    }

    if(m7_read_retry_mode == 0x30)
    {
        if((page!=255)&&((page==0)||((page)%2)))
        {
            dat = m7_19_low[nci->retry_count];
            addr = m7_read_retry_reg_adr_19;
            cnt = 3;
            m7_read_retry_cycle = 16;
        }
        else
        {
            dat = m7_19_high[nci->retry_count];
            addr = m7_read_retry_reg_adr_19;
            cnt = 3;
            m7_read_retry_cycle = 20;
        }
        ret = m7_vender_set_param(nci,dat,addr,cnt);
        set_one_cmd(nci,0xb6,0);

    }
    else if(m7_read_retry_mode == 0x31)
    {
        dat = m7_24[nci->retry_count];
        addr = m7_read_retry_reg_adr_24;
        cnt = 3;
        ret = m7_vender_set_param(nci,dat,addr,cnt);
        set_one_cmd(nci,0xb6,0);
    }
    else if(m7_read_retry_mode == 0x32)
    {
        dat = m7_1y[nci->retry_count];
        addr = m7_read_retry_reg_adr_1y;
        cnt = 1;
        ret = m7_vender_set_param(nci,dat,addr,cnt);
        set_one_cmd(nci,0x5d,0);
    }
    else
    {
        ;
    }

    if(m7_toggle_mode_flag == 1)
    {
        ndfc_set_toogle_interface(nci->nctri);  //change to toggle mode from legacy mode  after set param
    }

	nand_disable_chip(nci);

	m7_read_retry_clock_recover(nci);
	
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m7_exit_readretry(struct _nand_chip_info *nci)
{
    s32 ret = 0;
    u8 dat[4] = {0,0,0,0};
    u8* addr;
    u32 cnt;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    if(ndfc_is_toogle_interface(nci->nctri) != 0) //change to legacy mode from toggle mode after 0x53h cmd
    {
        ndfc_set_legacy_interface(nci->nctri);
        m7_toggle_mode_flag = 1;
    }

    if(m7_read_retry_mode == 0x30)
    {
        addr = m7_read_retry_reg_adr_19;
        cnt = 3;
        ret = m7_vender_set_param(nci,dat,addr,cnt);
    }
    else if(m7_read_retry_mode == 0x31)
    {
        addr = m7_read_retry_reg_adr_24;
        cnt = 3;
        ret = m7_vender_set_param(nci,dat,addr,cnt);
    }
    else if(m7_read_retry_mode == 0x32)
    {
        set_one_cmd(nci,0xff,1);
        addr = m7_read_retry_reg_adr_1y;
        cnt = 1;
        ret = m7_vender_set_param(nci,dat,addr,cnt);
    }
    else
    {
        ;
    }

    if(m7_toggle_mode_flag == 1)
    {
        ndfc_set_toogle_interface(nci->nctri);  //change to toggle mode from legacy mode  after set param
    }

	nand_disable_chip(nci);

//    m7_read_retry_clock_recover(nci);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m7_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m7_readretry_init(nci);
		nci = nci->nsi_next;
	}

    if(ret == 0)
    {
        function_read_page_end = m7_read_page_end;
	    PHY_DBG(" m7_special_init m7_read_retry_mode:%d m7_read_retry_cycle :%d m7_read_retry_reg_cnt %d \n",m7_read_retry_mode,m7_read_retry_cycle,m7_read_retry_reg_cnt);
    }
	else
	{
	    PHY_ERR(" m7_special_init error m7_read_retry_mode:%d m7_read_retry_cycle :%d m7_read_retry_reg_cnt %d \n",m7_read_retry_mode,m7_read_retry_cycle,m7_read_retry_reg_cnt);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m7_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
		m7_readretry_exit(nci);
		nci = nci->nsi_next;
	}
	PHY_DBG(" m7_special_exit \n");
    return 0;
}

int m7_is_lsb_page(__u32 page_num)
{	
	struct _nand_chip_info *nci = g_nsi->nci;

	//sandisk 2xnm 19nm 1ynm
	if(page_num==0)
		return 1;
	if(page_num==nci->npi->page_cnt_per_blk - 1)
		return 0;
	if(page_num%2==1)
		return 1;
	return 0;
}


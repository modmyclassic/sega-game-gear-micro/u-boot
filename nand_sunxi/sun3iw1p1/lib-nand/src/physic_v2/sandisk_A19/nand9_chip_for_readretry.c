/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR9_C_

#include "../nand_physic_inc.h"

u8 m9_read_retry_mode = 0;
u8 m9_read_retry_cycle = 0;
u8 m9_read_retry_reg_cnt = 0;


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
u8 m9_read_retry_enable_cmd_1y[1] = {0x5D};
u8 m9_read_retry_exit_cmd_1y[1] = {0xFF};

u8 m9_read_retry_cmd_1y[1] =  {0xEF};
u8 m9_read_retry_reg_adr_1y[1] = {0x11};
u8 m9_1y[31][4] ={
    {0x7c,0x00,0x00,0x7C},
    {0x04,0x00,0x7c,0x78},
    {0x78,0x00,0x78,0x74},
    {0x08,0x7C,0x00,0x7c},
    {0x00,0x7c,0x7c,0x78},
    {0x7c,0x7c,0x78,0x74},
    {0x00,0x7C,0x74,0x70},
    {0x00,0x78,0x00,0x7c},
    {0x00,0x78,0x7c,0x78},
    {0x00,0x78,0x78,0x74},
    {0x00,0x78,0x74,0x70},
    {0x00,0x78,0x70,0x6c},
    {0x00,0x04,0x04,0x00},
    {0x00,0x04,0x00,0x7c},
    {0x0c,0x04,0x7c,0x78},
    {0x0c,0x04,0x78,0x74},
    {0x10,0x08,0x00,0x7c},
    {0x10,0x08,0x04,0x00},
    {0x78,0x74,0x78,0x74},
    {0x78,0x74,0x74,0x70},
	{0x78,0x74,0x70,0x6c},
	{0x78,0x74,0x6c,0x68},
	{0x78,0x70,0x78,0x74},
	{0x78,0x70,0x74,0x70},
	{0x78,0x70,0x6c,0x68},
	{0x78,0x70,0x70,0x6c},
	{0x78,0x6c,0x70,0x6c},
	{0x78,0x6c,0x6c,0x68},
	{0x78,0x6c,0x68,0x64},
	{0x74,0x68,0x6c,0x68},
	{0x74,0x68,0x68,0x64}
};

u8 m9_1z_16g[32][4] ={  
	 {0x7C,0x00,0x04,0x00},// 1
	 {0x04,0x00,0x00,0x7C},// 2
	 {0x78,0x00,0x08,0x04},// 3
	 {0x08,0x00,0x7C,0x78},// 4
	 {0x7C,0x7C,0x04,0x00},// 5
	 {0x00,0x7C,0x00,0x7C},// 6
	 {0x78,0x7C,0x08,0x04},// 7
	 {0x04,0x7C,0x74,0x78},// 8
	 {0x04,0x04,0x04,0x00},// 9
	 {0x08,0x04,0x00,0x7C},// 10
	 {0x00,0x04,0x08,0x04},// 11
	 {0x0C,0x04,0x7C,0x78},// 12
	 {0x7C,0x04,0x0C,0x08},// 13
	 {0x78,0x78,0x04,0x00},// 14
	 {0x7C,0x78,0x00,0x7C},// 15
	 {0x74,0x78,0x70,0x78},// 16
	 {0x08,0x08,0x04,0x00},// 17
	 {0x04,0x08,0x08,0x04},// 18
	 {0x0C,0x08,0x0C,0x08},// 19
	 {0x00,0x00,0x0C,0x08},// 20
	 {0x04,0x00,0x78,0x74},// 21
	 {0x7C,0x7C,0x78,0x74},// 22
	 {0x78,0x78,0x78,0x74},// 23
	 {0x08,0x08,0x00,0x7C},// 24
	 {0x74,0x74,0x00,0x7C},// 25
	 {0x78,0x74,0x7C,0x78},// 26
	 {0x70,0x74,0x74,0x74},// 27
	 {0x7C,0x74,0x70,0x70},// 28
	 {0x0C,0x0C,0x08,0x04},// 29
	 {0x10,0x0C,0x0C,0x08},// 30
	 {0x70,0x70,0x70,0x74},// 31
	 {0x74,0x70,0x70,0x70} // 32
};

u8 m9_1z_8g[31][4] ={  
	 {0x04,0x04,0x78,0x78},// 1
	 {0x04,0x04,0x7c,0x74},// 2
	 {0x00,0x00,0x04,0x78},// 3
	 {0x04,0x04,0x7C,0x7c},// 4
	 {0x00,0x00,0x00,0x7c},// 5
	 {0x00,0x00,0x00,0x74},// 6
	 {0x08,0x08,0x04,0x78},// 7
	 {0x7c,0x7C,0x04,0x78},// 8
	 {0x7c,0x7c,0x00,0x7c},// 9
	 {0x04,0x04,0x7c,0x70},// 10
	 {0x7c,0x7c,0x74,0x74},// 11
	 {0x00,0x00,0x78,0x70},// 12
	 {0x0C,0x0c,0x08,0x78},// 13
	 {0x78,0x78,0x7c,0x7c},// 14
	 {0x04,0x04,0x08,0x04},// 15
	 {0x78,0x78,0x08,0x78},// 16
	 {0x7c,0x7c,0x78,0x70},// 17
	 {0x78,0x78,0x70,0x6c},// 18
	 {0x00,0x00,0x74,0x6c},// 19
	 {0x08,0x08,0x00,0x74},// 20
	 {0x7c,0x7c,0x78,0x6c},// 21
	 {0x00,0x00,0x04,0x04},// 22
	 {0x74,0x74,0x74,0x6c},// 23
	 {0x78,0x78,0x7c,0x70},// 24
	 {0x0c,0x0c,0x00,0x74},// 25
	 {0x04,0x04,0x0C,0x08},// 26
	 {0x78,0x78,0x7c,0x74},// 27
	 {0x78,0x78,0x70,0x68},// 28
	 {0x08,0x08,0x00,0x70},// 29
	 {0x10,0x10,0x0C,0x78},// 30
	 {0x00,0x00,0x0c,0x08},// 31
};

////////////////////////////////////////////////////////////

static u32 m9_sclk0_bak = 0;
static u32  m9_sclk1_bak = 0;
static u32  m9_toggle_mode_flag = 0;

s32 m9_exit_readretry(struct _nand_chip_info *nci);
extern int m9_end_one_read_retry(struct _nand_chip_info *nci);
extern int is_nouse_page(u8* buf);
s32 m9_set_readretry(struct _nand_chip_info *nci,u16 page);

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m9_dsp_on(struct _nand_physic_op_par* npo)
{
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

//    if(ndfc_is_toogle_interface(nci->nctri) != 0)
//    {
//        ndfc_set_legacy_interface(nci->nctri);
//        m9_toggle_mode_flag = 1;
//    }

    set_one_cmd(nci,0x26,0);

//    if(m9_toggle_mode_flag == 1)
//    {
//        ndfc_set_toogle_interface(nci->nctri);
//		m9_toggle_mode_flag = 0;
//    }

	nand_disable_chip(nci);
	return 0;
}

int m9_cmd25(struct _nand_physic_op_par* npo)
{
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

//    if(ndfc_is_toogle_interface(nci->nctri) != 0)
//    {
//        ndfc_set_legacy_interface(nci->nctri);
//        m9_toggle_mode_flag = 1;
//    }

    set_one_cmd(nci,0x25,0);

//    if(m9_toggle_mode_flag == 1)
//    {
//        ndfc_set_toogle_interface(nci->nctri);
//		m9_toggle_mode_flag = 0;
//    }

	nand_disable_chip(nci);
	return 0;
}

int m9_set_lmflgfix_next(struct _nand_physic_op_par* npo,__u8 value)
{
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);
	__u8 addr ;
	__u8 data ;
	__u8 cmd ;


    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

//    if(ndfc_is_toogle_interface(nci->nctri) != 0)
//    {
//        ndfc_set_legacy_interface(nci->nctri);
//        m9_toggle_mode_flag = 1;
//    }

	set_one_cmd(nci,0x5c,0);
	set_one_cmd(nci,0xc5,0);

	addr = 0x0;
	data = 0x1;
	cmd = 0x55;
	set_cmd_with_nand_bus(nci,&cmd,1,&addr,&data,1,1);//enter test mode


	if(0x34 == m9_read_retry_mode)
		addr = 0x22;
	else if(0x35 == m9_read_retry_mode)
		addr = 0x25;
	else
		addr = 0x23;
	data = value;
	cmd = 0x55;
	set_cmd_with_nand_bus(nci,&cmd,1,&addr,&data,1,1);//set lmflgfix_next

	addr = 0x0;
	data = 0x0;
	cmd = 0x55;
	set_cmd_with_nand_bus(nci,&cmd,1,&addr,&data,1,1);//exit test mode

//    if(m9_toggle_mode_flag == 1)
//    {
//        ndfc_set_toogle_interface(nci->nctri);
//		m9_toggle_mode_flag = 0;
//    }

	nand_disable_chip(nci);
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m9_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0,j, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);
    if(ret == ERR_ECC)
	{
	    PHY_DBG("m9 retry!\n");

	    m9_retry_case_0:  //dsp off
		for(i=0; i<m9_read_retry_cycle; i++)
		{
		    nci->retry_count = i;
		    ret = m9_set_readretry(nci,npo->page);
		    if(ret != 0)
		    {
		    	PHY_DBG("m9 set readretry error\n");
		        continue;
		    }

		    ret = m0_read_page_start(npo);
		    ret |= m0_read_page_end_not_retry(npo);

			if(is_nouse_page(npo->sdata) == 1)
			{
				ret = ERR_ECC;
				PHY_DBG("retry spare all 0xff! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				continue;
			}

		    if((ret == ECC_LIMIT) || (ret == 0))
		    {
				if(ret == 0)
					PHY_DBG("m9 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				else
					PHY_DBG("m9 ReadRetry ok with ecc limit! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);

                ret = ECC_LIMIT;
				goto m9_retry_end;
		    }
		}

	    if(m9_read_retry_mode == 0x33)
	    {
	        goto m9_retry_case_3;
	    }

	    m9_retry_case_1:  //dsp  on
		for(i=0; i<m9_read_retry_cycle; i++)
		{
		    nci->retry_count = i;
		    ret = m9_set_readretry(nci,npo->page);
		    if(ret != 0)
		    {
		    	PHY_DBG("m9 set readretry error\n");
		        continue;
		    }
            m9_dsp_on(npo);
		    ret = m0_read_page_start(npo);
		    ret |= m0_read_page_end_not_retry(npo);
			if(is_nouse_page(npo->sdata) == 1)
			{
				ret = ERR_ECC;
				PHY_DBG("retry spare all 0xff! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				continue;
			}
		    if((ret == ECC_LIMIT) || (ret == 0))
		    {
				if(ret == 0)
					PHY_DBG("m9 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				else
					PHY_DBG("m9 ReadRetry ok with ecc limit! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);

                ret = ECC_LIMIT;
				goto m9_retry_end;
		    }
		}

	    m9_retry_case_2: //cmd25h,dsp off
		for(i=0; i<m9_read_retry_cycle; i++)
		{
		    nci->retry_count = i;
		    ret = m9_set_readretry(nci,npo->page);
		    if(ret != 0)
		    {
		    	PHY_DBG("m9 set readretry error\n");
		        continue;
		    }
			m9_cmd25(npo);
			
		    ret = m0_read_page_start(npo);
		    ret |= m0_read_page_end_not_retry(npo);

			if(is_nouse_page(npo->sdata) == 1)
			{
				ret = ERR_ECC;
				PHY_DBG("retry spare all 0xff! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				continue;
			}

		    if((ret == ECC_LIMIT) || (ret == 0))
		    {
				if(ret == 0)
					PHY_DBG("m9 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				else
					PHY_DBG("m9 ReadRetry ok with ecc limit! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);

                ret = ECC_LIMIT;
				goto m9_retry_end;
		    }
		}

	    m9_retry_case_3: //cmd25h,dsp on,LMFLGFIX_NEXT=1
		for(i=0; i<m9_read_retry_cycle; i++)
		{
		    nci->retry_count = i;
		    ret = m9_set_readretry(nci,npo->page);
		    if(ret != 0)
		    {
		    	PHY_DBG("m9 set readretry error\n");
		        continue;
		    }

			if(m9_read_retry_mode == 0x34)
				m9_set_lmflgfix_next(npo,0xc6);
			else if(m9_read_retry_mode == 0x35)
				m9_set_lmflgfix_next(npo,0x1a);
			else
				m9_set_lmflgfix_next(npo,0xc0);
			m9_cmd25(npo);
			if((m9_read_retry_mode == 0x32)||(m9_read_retry_mode == 0x34)||(m9_read_retry_mode == 0x35))
				m9_dsp_on(npo);

		    ret = m0_read_page_start(npo);
		    ret |= m0_read_page_end_not_retry(npo);

			if(m9_read_retry_mode == 0x34)
				m9_set_lmflgfix_next(npo,0xc2);
			else if(m9_read_retry_mode == 0x35)
				m9_set_lmflgfix_next(npo,0x0a);
			else
				m9_set_lmflgfix_next(npo,0x40);
			if(is_nouse_page(npo->sdata) == 1)
			{
				ret = ERR_ECC;
				PHY_DBG("retry spare all 0xff! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				continue;
			}
		    if((ret == ECC_LIMIT) || (ret == 0))
		    {
				if(ret == 0)
					PHY_DBG("m9 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
				else
					PHY_DBG("m9 ReadRetry ok with ecc limit! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);

                ret = ECC_LIMIT;
				goto m9_retry_end;
		    }
		}

m9_retry_end:
		nci->retry_count = 0;
        m9_exit_readretry(nci);
	}

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m9_readretry_init(struct _nand_chip_info *nci)
{
	nci->retry_count = 0;

    m9_read_retry_mode = (nci->npi->read_retry_type >> 16) & 0xff;
    m9_read_retry_cycle = (nci->npi->read_retry_type >> 8) & 0xff;
    m9_read_retry_reg_cnt = nci->npi->read_retry_type & 0xff;

    if((m9_read_retry_mode != 0x32) && (m9_read_retry_mode != 0x33) && (m9_read_retry_mode != 0x34) && (m9_read_retry_mode != 0x35))
    {
		PHY_ERR("m9 retry mode err : %d !\n",m9_read_retry_mode);
		return ERR_NO_99;
    }

    PHY_DBG("sandisk A19 read retry conut: %d !\n",m9_read_retry_cycle);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m9_readretry_exit(struct _nand_chip_info *nci)
{
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m9_read_retry_clock_save(struct _nand_chip_info *nci)
{
    NAND_GetClk(nci->nctri->channel_id, &m9_sclk0_bak, &m9_sclk1_bak);
    NAND_SetClk(nci->nctri->channel_id, 10, 10*2);
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m9_read_retry_clock_recover(struct _nand_chip_info *nci)
{
    NAND_SetClk(nci->nctri->channel_id, m9_sclk0_bak, m9_sclk1_bak);
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m9_vender_set_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    s32 ret = 0;

	ret = set_cmd_with_nand_bus(nci,m9_read_retry_cmd_1y,1,addr,para,4,1);

//	PHY_DBG("rr value %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m9_set_readretry(struct _nand_chip_info *nci,u16 page)
{
    s32 ret = 0;
    u8* dat;
    u8* addr;
    u32 cnt;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    //if(ndfc_is_toogle_interface(nci->nctri) != 0)
    //{
    //   ndfc_set_legacy_interface(nci->nctri);
    //    m9_toggle_mode_flag = 1;
    //}
	if (m9_read_retry_mode == 0x34)
		dat = m9_1z_16g[nci->retry_count];
	else if (m9_read_retry_mode == 0x35)
		dat = m9_1z_8g[nci->retry_count];
	else
    	dat = m9_1y[nci->retry_count];
    addr = m9_read_retry_reg_adr_1y;
    cnt = 1;
    ret = m9_vender_set_param(nci,dat,addr,cnt);
    set_one_cmd(nci,0x5d,0);


    //if(m9_toggle_mode_flag == 1)
    //{
    //    ndfc_set_toogle_interface(nci->nctri);
	//	m9_toggle_mode_flag = 0;
    //}

	nand_disable_chip(nci);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m9_exit_readretry(struct _nand_chip_info *nci)
{
    s32 ret = 0;
    u8 dat[4] = {0,0,0,0};
    u8* addr;
    u32 cnt;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

//    if(ndfc_is_toogle_interface(nci->nctri) != 0)
//    {
//        ndfc_set_legacy_interface(nci->nctri);
//        m9_toggle_mode_flag = 1;
//    }

    set_one_cmd(nci,0xff,1);
    addr = m9_read_retry_reg_adr_1y;
    cnt = 1;
    ret = m9_vender_set_param(nci,dat,addr,cnt);

//    if(m9_toggle_mode_flag == 1)
//    {
//        ndfc_set_toogle_interface(nci->nctri);
//		m9_toggle_mode_flag = 0;
//    }
	nand_disable_chip(nci);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m9_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m9_readretry_init(nci);
		nci = nci->nsi_next;
	}

    if(ret == 0)
    {
        function_read_page_end = m9_read_page_end;
	    PHY_DBG(" m9_special_init m9_read_retry_mode:%d m9_read_retry_cycle :%d m9_read_retry_reg_cnt %d \n",m9_read_retry_mode,m9_read_retry_cycle,m9_read_retry_reg_cnt);
    }
	else
	{
	    PHY_ERR(" m9_special_init error m9_read_retry_mode:%d m9_read_retry_cycle :%d m9_read_retry_reg_cnt %d \n",m9_read_retry_mode,m9_read_retry_cycle,m9_read_retry_reg_cnt);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m9_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;
	int ret;

	while(nci != NULL)
	{
		m9_readretry_exit(nci);
		if (nci->support_vendor_specific_cfg)
		{
			nci->interface_type = SDR;
			ret = _setup_nand_toggle_vendor_specific_feature(nci);
			if (ret)
			{
				PHY_ERR("_setup_nand_toggle_vendor_specific_feature() failed!\n");
				return ret;
			}
		}
		nci = nci->nsi_next;
	}
	PHY_DBG(" m9_special_exit \n");
    return 0;
}

int m9_check_bad_block_first_burn(struct _nand_physic_op_par* npo)
{
    int ret;
    unsigned int row_addr = 0, col_addr = 0;
    struct _nand_chip_info* nci = nci_get_from_nsi(g_nsi,npo->chip);
    struct _nand_controller_info *nctri = nci->nctri;
    struct _nctri_cmd_seq* cmd_seq = &nci->nctri->nctri_cmd_seq;

//	PHY_DBG("%s: ch: %d  chip: %d/%d  block: %d/%d \n", __func__, nctri->channel_id, nci->nctri_chip_no, nctri->chip_cnt, npo->block, nci->blk_cnt_per_chip);

    if ((nci->nctri_chip_no >= nctri->chip_cnt) || (npo->block >= nci->blk_cnt_per_chip))
    {
		PHY_ERR("fatal err -0, wrong input parameter, ch: %d  chip: %d/%d  block: %d/%d \n", nctri->channel_id, nci->nctri_chip_no, nctri->chip_cnt, npo->block, nci->blk_cnt_per_chip);
		return ERR_NO_10;
	}
	ndfc_disable_randomize(nci->nctri);
	//wait nand ready before erase
	nand_read_chip_status_ready(nci);

    nand_enable_chip(nci);

    ndfc_clean_cmd_seq(cmd_seq);

    // cmd1: 0xa2
    cmd_seq->cmd_type = CMD_TYPE_NORMAL;
    cmd_seq->nctri_cmd[0].cmd = 0xA2;
    cmd_seq->nctri_cmd[0].cmd_valid = 1;
    cmd_seq->nctri_cmd[0].cmd_send = 1;
	cmd_seq->nctri_cmd[0].cmd_wait_rb = 0;

    // cmd2: 0x80
    cmd_seq->nctri_cmd[1].cmd = CMD_WRITE_PAGE_CMD1;
    cmd_seq->nctri_cmd[1].cmd_valid = 1;
    cmd_seq->nctri_cmd[1].cmd_send = 1;
    cmd_seq->nctri_cmd[1].cmd_wait_rb = 0;

	row_addr = get_row_addr(nci->page_offset_for_next_blk, npo->block, npo->page);
    cmd_seq->nctri_cmd[1].cmd_acnt = 5;
    fill_cmd_addr(col_addr, 2, row_addr, 3, cmd_seq->nctri_cmd[1].cmd_addr);

	// cmd2: 0x10
    cmd_seq->nctri_cmd[2].cmd = CMD_WRITE_PAGE_CMD2;
    cmd_seq->nctri_cmd[2].cmd_valid = 1;
    cmd_seq->nctri_cmd[2].cmd_send = 1;
    cmd_seq->nctri_cmd[2].cmd_wait_rb = 1;

    ret = ndfc_execute_cmd(nci->nctri, cmd_seq);

	ret = nand_read_chip_status_ready(nci);

    nand_disable_chip(nci);

    return ret;
}


